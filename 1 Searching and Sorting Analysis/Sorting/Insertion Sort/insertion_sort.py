# Apply Insertion Sort

A = [6,5,3,1,8,7,2,4]

def insertion_sort(A: int) -> None:
    # n = len(A)
    # if n <= 1: return

    # walrus opeartor -> named expression -> NAME := expr 
    # After acceptance of PEP 572 https://peps.python.org/pep-0572/ , python-3.8 introduced assignment expressions, which are a way to assign to variables within an expression using the notation NAME := expr
    if (n:= len(A)) <= 1: return
    # i can use the n later

    # for j in range(1, len(A)):
    for j in range(1, n):
        key = A[j] 
        i = j - 1
        while i >= 0 and A[i] > key:
            A[i+1] = A[i]
            i -= 1
        A[i+1] = key

insertion_sort(A)
print(A)