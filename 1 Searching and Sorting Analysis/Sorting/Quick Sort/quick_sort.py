# p = first index, r = last index
def partition(arr, p, r):
    # choose pivot
    pivot = arr[p]
    i = p
    for j in range(p+1, r+1): # r+1 excluded i.e. till r
        if arr[j] < pivot:
            i += 1
            arr[i], arr[j] = arr[j], arr[i]
    arr[i], pivot = pivot, arr[i] # not working, debug this
    # arr[i], arr[p] = arr[p], arr[i] # working 
    return i # index of the pivot

def quicksort(arr, p, r):
    if p < r:
        q = partition(arr, p, r)
        quicksort(arr, p, q-1)
        quicksort(arr, q+1, r)

arr= [54, 26, 93, 17, 77, 31, 44, 55]
quicksort(arr, 0, len(arr)-1)
print(arr)

# worst case = O(n2)
# best case = O(nlogn)