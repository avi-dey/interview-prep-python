# at every pass i need to find the smallest element and swap it at the beginning of the unsorted part

def selection_sort(arr):
    # the last element is auto sorted, thus we only need to go till 2nd last element
    for i in range(len(arr)-1):
        # assume the first element to be smallest
        i_min = i
        for j in range(i+1, len(arr)):
            if arr[j] < arr[i_min]:
                i_min = j
        # swap with the first element of the unsorted part
        # no need to swap if the i th element was the smallest
        if (i != i_min):
            arr[i], arr[i_min] = arr[i_min], arr[i]

arr = [14,51,61,34,13,12,52,62]
selection_sort(arr)
print(arr)
