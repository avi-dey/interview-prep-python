# this code needs testing with timeit function

arr = [4,1,5,3,8]

# adding a flag to check if the loop is already sorted at some point, then we shall not waste any more time
swapped = True # to execute the outer loop for the first time
i = 0
while swapped or i < len(arr):
    swapped = False
    for j in range(len(arr)-i-1):
        if arr[j] > arr[j+1]:
            arr[j], arr[j+1] = arr[j+1], arr[j]
            swapped = True
    i = i + 1

print(arr)
