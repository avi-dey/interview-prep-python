arr = [4,1,5,3,8]
for i in range(len(arr)):
    for j in range(len(arr)-i-1): # -i because the end of the array gets sorted after every iteration of the outer loop or after every complete execution of the inner loop
        if arr[j] > arr[j+1]:
            # swap
            arr[j], arr[j+1] = arr[j+1], arr[j]

print(arr)
