arr = [23,1,3,54,23,63,12,64,34,75,1,32,53]
# binary search only works on sorted lists
arr.sort()
print(f"our array is : {arr}")

# time comp : O(logn)
# space comp : O(1) auxiliary

def binary_search(arr,target):
    start = 0
    end = len(arr)-1

    while start <= end:
        mid = (start + end) // 2

        if target == arr[mid]:
            return mid
        elif target < arr[mid]:
            end = mid - 1
        else:
            start = mid + 1
    
    # this will run if the return statement inside while loop doesn't run
    # i.e. target not found
    return -1

ans = binary_search(arr, 32)
print(f"found at index: {ans}")