arr = [23,1,3,54,23,63,12,64,34,75,1,32,53]
# binary search only works on sorted lists
arr.sort()
print(f"our array is : {arr}")

# time comp : O(logn)
# space comp : O(logn) auxiliary -- creates call stacks

# search for target
def binary_search(arr, target, start, end):
    if start<= end :
        mid = (start + end) // 2
        if target == arr[mid]:
            return mid
        elif target < arr[mid]:
            return binary_search(arr, target, start, mid-1) # left
        else:
            return binary_search(arr, target, mid+1, end) # right
    else:
        return -1

ans = binary_search(arr, 32, 0, len(arr)-1)
print(f"found at index: {ans}")
    