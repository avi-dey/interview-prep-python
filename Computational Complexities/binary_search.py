from random import shuffle
from math import floor

# implement binary search - non recursive
# return the index
def binary_search(lst , query):
    start = 0
    end = len(lst) - 1 

    while(start <= end):
        # mid index
        mid = int((start + end) /2)
        # mid = floor( (start + end)/2 ) #same  

        if query == lst[mid]:
            return mid # query found
        elif query < lst[mid]:
            end = mid - 1
        else:
            start = mid + 1
    return -1 # not found


l = list(range(100))
shuffle(l)
q = 55
l.sort() # list must be sorted to perform binary search
# l and q is given to you, that doesnot count as extra memory

ans = binary_search(l, q)
print(ans)