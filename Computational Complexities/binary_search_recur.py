from math import floor

# return index of the element
def binary_search_recur(lst, query, start, end):
    mid = floor((start + end) / 2)
    
    # base case
    if (query == lst[mid]): return mid
    
    if (query < lst[mid]): return binary_search_recur(lst, query, start, mid-1)
    
    # remaining case
    return binary_search_recur(lst, query, mid+1, end)

l = list(range(51))
q = 34
ans = binary_search_recur(l, q, 0, len(l)-1)
print(ans)
